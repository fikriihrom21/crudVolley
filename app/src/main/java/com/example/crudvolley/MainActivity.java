package com.example.crudvolley;

import static com.android.volley.VolleyLog.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String url = "http://192.168.1.11/Crudvolley/insert.php";
    EditText tnim, tnama, talamat;
    TextView tKeterangan;
    Button btnInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tnim = (EditText) findViewById(R.id.inNim);
        tnama = (EditText) findViewById(R.id.inNama);
        talamat = (EditText) findViewById(R.id.inAlamat);
        tKeterangan = (TextView) findViewById(R.id.txtKeterangan);
        btnInput =(Button) findViewById(R.id.btnInput);

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputData();
            }
        });
    }

    void inputData(){
        String nim = tnim.getText().toString();
        if (nim != null){
            Log.d(TAG, "inputData: nim kosong");
        }
        String nama = tnama.getText().toString();
        if (nama != null){
            Log.d(TAG, "inputData: nama kosong");
        }
        String alamat = talamat.getText().toString();
        if (alamat != null){
            Log.d(TAG, "inputData: alamat kosong");
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        tKeterangan.setText(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    tKeterangan.setText("ERROR");
            }
        })

        {
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                //posting parameters he post url
                Map<String, String> params = new HashMap<>();

                params.put("nim", nim);
                params.put("nama", nama);
                params.put("alamat", alamat);

                return params;

            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);


    }

}